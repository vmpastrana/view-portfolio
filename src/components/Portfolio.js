import React, {useState, useEffect} from "react";
import './Portfolio.css';
import {Card, ListGroup} from "react-bootstrap";
import axios from 'axios';

export default function Porfolio() {

  const [persons, setPersons] = useState([]);
  const [tweets, setTweets] = useState([]);

  useEffect(()=> {
    axios.get(`http://localhost:8090/portfolios/15`)
      .then(res => {
        setTweets(res.data.tweets);
        setPersons(res.data);
      })
  },[]);

    return (
      <div>
        <h1>Portfolio</h1>
        <Card style={{ width: '35rem' }}>
          <Card.Body>
            <Card.Title>{persons.name} {persons.lastName}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{persons.tittle} - @{persons.twitUserName}</Card.Subtitle>
            <div>
              <Card.Img variant="top" src={persons.image} />
              <Card.Text class="modify">
                {persons.description}
              </Card.Text>
            </div>
            <ListGroup>
              {tweets.map((twt, index) =>
                  <ListGroup.Item variant="light" key={index}>
                    <Card.Img className="card-img-group" src={twt.profileImageUrl} />
                    {twt.text}
                    <footer className="footer">
                      {twt.createdAt.substring(0,19).replace('T',' ')}
                    </footer>
                  </ListGroup.Item>
              )}
            </ListGroup>
          </Card.Body>
        </Card>
      </div>
    )
}
